class WebsiteSection extends HTMLElement {
    constructor() {
      super();
  
      // Create a shadow DOM for the component
      this.attachShadow({ mode: 'open' });
  
      // Get the template content
      const template = document.querySelector('#website-section-template');
      this.shadowRoot.appendChild(template.content.cloneNode(true));
  
      // Elements within the shadow DOM
      this.titleElement = this.shadowRoot.getElementById('title');
      this.descriptionElement = this.shadowRoot.getElementById('description');
      this.innerContentElement = this.shadowRoot.getElementById('inner-content');
    }
  
    connectedCallback() {
      // Set initial attributes and content
      this.title = this.getAttribute('title');
      this.description = this.getAttribute('description');
      this.innerContent = this.innerHTML;
  
      // Update the shadow DOM
      this.render();
    }
  
    render() {
      // Update the component's elements
      this.titleElement.textContent = this.title;
      this.descriptionElement.textContent = this.description;
      this.innerContentElement.innerHTML = this.innerContent;
    }
  
    // Define getters and setters for attributes
    get title() {
      return this.getAttribute('title');
    }
    set title(value) {
      this.setAttribute('title', value);
    }
  
    get description() {
      return this.getAttribute('description');
    }
    set description(value) {
      this.setAttribute('description', value);
    }
  
    get innerContent() {
      return this.innerHTML;
    }
    set innerContent(value) {
      this.innerHTML = value;
      this.render();
    }
  }
  
  customElements.define('website-section', WebsiteSection);
  
  